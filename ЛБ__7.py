#Цель: Протабулировать функцию при изменении аргумента с постоянным шагом.
#Переменные: x0(int)-начальное значение аргумента; xn(int)-конечное значение аргумента;
#hx(int)-постояннй шаг; x(int)-меняющееся значение аргумента функции.
#Метод: Цикл с постусловием.
#Дата написания: 05.10.2023.
#Программист:

#Ввод исходных данных
x0 = int(input('Введите начальное значение x0:'))
xn = int(input('Введите конечное значение xn:'))
hx = int(input('Введите постоянный шаг hx:'))
print('Вы ввели x0={}, xn={}, hx={}'.format(x0,xn,hx))

#Массив для вывода полученных значений
A=[]
#Аргумент функции
x=x0

#Проверка исходных данных на совместимость
if x0<xn and hx>0:
    #Цикл с постусловием
    while True:
        #Вычисление соответствующих значений функции
        if x<=-2: A.append('x={}, f(x)={:.3f}'.format(x, 8/x))
        elif -2<x<=0: A.append('x={}, f(x)={:.3f}'.format(x,x**3 + 4))
        else: A.append('x={}, f(x)={:.3f}'.format(x,4/(x**2 + 1)))

        #Изменение аргумента на шаг hx
        x += hx

        #Выход из цикла с постусловием
        if x > xn + hx / 2: break

    #Вывод полученных значений
    print(A)

else:
    exit('В исходных данных содержится ошибка')
